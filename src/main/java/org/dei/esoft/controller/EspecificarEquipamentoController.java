package org.dei.esoft.controller;

import java.util.List;
import org.dei.esoft.model.Empresa;
import org.dei.esoft.model.Equipamento;

/**
 *
 * @author 1060756_1171247
 */
public class EspecificarEquipamentoController {

    private Empresa empresa;
    private Equipamento equipamento;

    public EspecificarEquipamentoController(Empresa empresa) {
        this.empresa = empresa;
    }

    public void novoEquipamento() {
        this.equipamento = this.empresa.novoEquipamento();
    }

    public void setDados(String descricao, String enderecoLogico, String enderecoFisico, String ficheiro) {
        this.equipamento.setDescricao(descricao);
        this.equipamento.setEnderecoLogico(enderecoLogico);
        this.equipamento.setEnderecoFisico(enderecoFisico);
        this.equipamento.setFicheiroConfiguracao(ficheiro);
    }

    public boolean registaEquipamento() {
        return this.empresa.registaEquipamento(this.equipamento);
    }

    public String getEquipamentoToString() {
        return this.equipamento.toString();
    }

    public void validaEquipamento() {
        this.empresa.validaEquipamento(this.equipamento);
    }

    public List<Equipamento> getListaEquipamentos() {
        return this.empresa.getListaEquipamentos();
    }

    public List<Equipamento> getListaEquipamentosToString() {
        return this.empresa.getListaEquipamentos();
    }

    public String getEnderecoLogico(Equipamento equipamento) {
        return equipamento.getEnderecoLogico();
    }
}

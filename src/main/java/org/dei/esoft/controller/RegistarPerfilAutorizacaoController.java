package org.dei.esoft.controller;

import org.dei.esoft.model.Empresa;
import org.dei.esoft.model.Equipamento;
import org.dei.esoft.model.PerfilAutorizacao;
import org.dei.esoft.model.PeriodoAutorizacao;
import org.dei.esoft.model.RegistoEquipamentos;

/**
 *
 * @author 1060756_1171247
 */
public class RegistarPerfilAutorizacaoController {

    private Empresa empresa;
    private Equipamento equipamento;
    private PerfilAutorizacao perfilAutorizacao;
    private RegistoEquipamentos registoEquipamentos;
    public PeriodoAutorizacao PeriodoAutorizacao;

    public RegistarPerfilAutorizacaoController(Empresa empresa) {
        this.empresa = empresa;
    }

    public void novoPerfilAutorizacao() {

    }

    public void setDados(String identificador, String descricao) {
        this.perfilAutorizacao.setIdentificacao(identificador);
        this.perfilAutorizacao.setDescricao(descricao);
    }

    public boolean addEquipamento(String identificacaoEquipamento) {
        this.equipamento = registoEquipamentos.getEquipamento(identificacaoEquipamento);
        return this.perfilAutorizacao.adicionarEquipamento(this.equipamento);
    }

    public boolean addPeriodoAutorizacao(PeriodoAutorizacao periodoAutorizacao) {
        return this.perfilAutorizacao.adicionarPeriodoAutorizacao(periodoAutorizacao);
    }

    public boolean validaPerfilAutorizacao() {
        return this.empresa.validaPerfilAutorizacao(this.perfilAutorizacao);
    }

    public boolean adicionarPerfilAutorizacao() {
        return this.empresa.registaPerfilAutorizacao(this.perfilAutorizacao);
    }

    public String getPerfilAutorizacaoString() {
        return this.perfilAutorizacao.toString();
    }

    public void novoEquipamentoAutorizado(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

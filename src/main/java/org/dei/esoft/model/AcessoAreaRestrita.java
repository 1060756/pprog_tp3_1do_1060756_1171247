/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dei.esoft.model;

/**
 *
 * @author Silvano Moura
 */
public class AcessoAreaRestrita {

    private String data;
    private String hora;
    private String movimento;
    private boolean sucesso;

    public AcessoAreaRestrita(String data, String hora, String movimento, boolean sucesso) {
        this.data = data;
        this.hora = hora;
        this.movimento = movimento;
        this.sucesso = sucesso;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * @return the movimento
     */
    public String getMovimento() {
        return movimento;
    }

    /**
     * @param movimento the movimento to set
     */
    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    /**
     * @return the sucesso
     */
    public boolean getSucesso() {
        return false;
    }

    /**
     * @param sucesso the sucesso to set
     */
    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    @Override
    public String toString() {
        return data + ";" + hora + ";" + movimento + ";" + sucesso;
    }
}

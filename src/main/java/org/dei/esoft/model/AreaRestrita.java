package org.dei.esoft.model;

/**
 *
 * @author 1060756_1171247
 */
public class AreaRestrita {

    int codigo;
    int lotacaoMaxima;
    String descricao;
    String localizacao;

    public AreaRestrita(int codigo, String descricao, String localizacao, int lotacaoMaxima) {
        setCodigo(codigo);
        setDescricao(descricao);
        setLocalizacao(localizacao);
        setLotacaoMaxima(lotacaoMaxima);
    }

    public int getCodigo() {
        return this.codigo;
    }

    public final void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public final void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLocalizacao() {
        return this.localizacao;
    }

    public final void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public int getLotacaoMaxima() {
        return this.lotacaoMaxima;
    }

    public final void setLotacaoMaxima(int lotacaoMaxima) {
        this.lotacaoMaxima = lotacaoMaxima;
    }

    @Override
    public String toString() {
        return codigo + ";" + descricao + ";" + localizacao + ";" + lotacaoMaxima;
    }
}

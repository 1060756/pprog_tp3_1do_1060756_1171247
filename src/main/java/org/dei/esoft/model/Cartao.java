package org.dei.esoft.model;

/**
 *
 * @author 1060756_1171247
 */
public class Cartao {

    private int id;
    private String dataEmissao;
    private String versao;

    private static final int ID_POR_OMISSAO = 0;
    private static final String DATAEMISSAO_POR_OMISSAO = "sem data";
    private static final String VERSAO_POR_OMISSAO = "sem versao";

    public Cartao(int id, String dataEmissao, String versao) {
        this.id = id;
        this.dataEmissao = dataEmissao;
        this.versao = versao;
    }

    public Cartao() {
        id = ID_POR_OMISSAO;
        dataEmissao = DATAEMISSAO_POR_OMISSAO;
        versao = VERSAO_POR_OMISSAO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(String dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    @Override
    public String toString() {
        return id + ";" + dataEmissao + ";" + versao;
    }
}

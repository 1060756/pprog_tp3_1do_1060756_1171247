package org.dei.esoft.model;

/**
 *
 * @author 1060756_1171247
 */
public class Colaborador {

    private int numeroMecanografico;
    private String nomeCompleto;
    private String nomeAbreviado;

    private static final int NUMEROMECANOGRAFICO_POR_OMISSAO = 0;
    private static final String NOMECOMPLETO_POR_OMISSAO = "sem nome";
    private static final String NOMEABREVIADO_POR_OMISSAO = "sem nome";

    public Colaborador(int numeroMecanografico, String nomeCompleto, String nomeAbreviado) {
        this.numeroMecanografico = numeroMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado;
    }

    public Colaborador() {
        numeroMecanografico = NUMEROMECANOGRAFICO_POR_OMISSAO;
        nomeCompleto = NOMECOMPLETO_POR_OMISSAO;
        nomeAbreviado = NOMEABREVIADO_POR_OMISSAO;
    }

    public int getNumeroMecanografico() {
        return numeroMecanografico;
    }

    public void setNumeroMecanografico(int numeroMecanografico) {
        this.numeroMecanografico = numeroMecanografico;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }

    @Override
    public String toString() {
        return numeroMecanografico + ";" + nomeCompleto + ";" + nomeAbreviado;
    }
}

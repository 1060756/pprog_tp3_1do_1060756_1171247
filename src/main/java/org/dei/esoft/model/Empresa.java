package org.dei.esoft.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1060756_1171247
 */
public class Empresa {

    private RegistoPerfisAutorizacao registoPerfisAutorizacao;
    
    private RegistoEquipamentos registoEquipamentos;

    private final List<Equipamento> listaEquipamentos;

    public Empresa() {
        this.listaEquipamentos = new ArrayList<Equipamento>();

        fillInData();
    }

    private void fillInData() {
        // Dados de Teste
        // Preenche alguns Terminais
        for (Integer i = 1; i <= 4; i++) {
            addEquipamento(new Equipamento("Equipamento " + i.toString(), "0" + i.toString(), "", ""));
        }
        //Preencher outros dados aqui
    }

    public Equipamento novoEquipamento() {
        return new Equipamento();
    }

    public boolean validaEquipamento(Equipamento equipamento) {
        boolean bRet = false;
        if (equipamento.valida()) {
            // Escrever aqui o código de validação
            bRet = true;
        }
        return bRet;
    }

    public boolean registaEquipamento(Equipamento equipamento) {
        if (this.validaEquipamento(equipamento)) {
            return addEquipamento(equipamento);
        }
        return false;
    }

    private boolean addEquipamento(Equipamento equipamento) {
        return this.listaEquipamentos.add(equipamento);
    }

    public List<Equipamento> getListaEquipamentos() {
        return this.listaEquipamentos;
    }

    public Equipamento getEquipamento(String equipamento) {
        for (Equipamento term : this.listaEquipamentos) {
            if (term.isIdentifiableAs(equipamento)) {
                return term;
            }
        }

        return null;
    }

    public RegistoPerfisAutorizacao getRegistoPerfisAutorizacao() {
        return new RegistoPerfisAutorizacao(this.registoPerfisAutorizacao);
    }

    public RegistoEquipamentos getRegistoEquipamentos() {
        return new RegistoEquipamentos(this.registoEquipamentos);
    }

    public boolean validaPerfilAutorizacao(PerfilAutorizacao perfilAutorizacao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean registaPerfilAutorizacao(PerfilAutorizacao perfilAutorizacao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

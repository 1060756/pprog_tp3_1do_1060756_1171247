package org.dei.esoft.model;

/**
 *
 * @author 1060756_1171247
 */
public class Equipamento {

    private String descricao;
    private String enderecoLogico;
    private String enderecoFisico;
    private String ficheiroConfiguracao;

    public Equipamento() {
    }

    public Equipamento(String descricao, String enderecoLogico, String enderecoFisico, String ficheiroConfiguracao) {
        this.setDescricao(descricao);
        this.setEnderecoLogico(enderecoLogico);
        this.setEnderecoFisico(enderecoFisico);
        this.setFicheiroConfiguracao(ficheiroConfiguracao);
    }

    public final void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public final void setEnderecoLogico(String enderecoLogico) {
        this.enderecoLogico = enderecoLogico;
    }

    public String getEnderecoLogico() {
        return this.enderecoLogico;
    }

    public final void setEnderecoFisico(String enderecoFisico) {
        this.enderecoFisico = enderecoFisico;
    }

    public String getEnderecoFisico() {
        return this.enderecoFisico;
    }

    public final void setFicheiroConfiguracao(String ficheiroConfiguracao) {
        this.ficheiroConfiguracao = ficheiroConfiguracao;
    }

    public boolean valida() {
        // Escrever aqui o código de validação
        return true;
    }

    public boolean isIdentifiableAs(String ID) {
        return this.enderecoLogico.equals(ID);
    }

    @Override
    public String toString() {
        return this.descricao + ";" + this.enderecoLogico + ";" + this.enderecoFisico + ";";
    }

    public void adicionarPerfilAutorizacao(PerfilAutorizacao perfilAutorizacao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

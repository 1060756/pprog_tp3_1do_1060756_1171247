package org.dei.esoft.model;

import java.util.ArrayList;

/**
 *
 * @author 1060756_1171247
 */
public class EquipamentosAutorizados {
    
    String enderecoLogico;
    private ArrayList<PeriodoAutorizacao> listaPeriodosAutorizacao;
    
    public EquipamentosAutorizados(String enderecoLogico){
        this.listaPeriodosAutorizacao = new ArrayList<>();
        this.setEquipamento(enderecoLogico);
    }
    
    public void setEquipamento(String enderecoLogico){
        this.enderecoLogico=enderecoLogico;
    }
    
    public void novoPeriodoAutorizacao(int hora_inicio, int hora_fim, String diaSemana){
        PeriodoAutorizacao periodoAutorizacao = new PeriodoAutorizacao (diaSemana, hora_inicio, hora_fim);
        this.listaPeriodosAutorizacao.add(periodoAutorizacao);
    }
}

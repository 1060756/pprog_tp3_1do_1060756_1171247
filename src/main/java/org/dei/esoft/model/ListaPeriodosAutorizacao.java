package org.dei.esoft.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Representa uma lista de periodos de autorização e é responsável por gerir e
 * registar os periodos de autorização de um equipamento.
 *
 * @author 1060756_1171247
 */
public class ListaPeriodosAutorizacao implements Serializable {

    /**
     * Lista de periodos de autorização.
     */
    private List<PeriodoAutorizacao> listaPeriodosAutorizacao;

    /**
     * Constrói uma instância de lista de periodos de autorização.
     */
    public ListaPeriodosAutorizacao() {
        listaPeriodosAutorizacao = new ArrayList<>();
    }

    /**
     * Constrói uma instância de lista de periodos de autorização recebendo uma
     * lista de periodos de autorização.
     *
     * @param listaPeriodosAutorizacao lista de periodos de autorização.
     */
    public ListaPeriodosAutorizacao(List<PeriodoAutorizacao> listaPeriodosAutorizacao) {
        this.listaPeriodosAutorizacao = new ArrayList<>(listaPeriodosAutorizacao);
    }

    /**
     * Constrói uma instância de lista de periodos de autorização copiando uma
     * lista de periodos de autorização.
     *
     * @param listaPeriodosAutorizacao lista de periodos de autorização.
     */
    public ListaPeriodosAutorizacao(ListaPeriodosAutorizacao listaPeriodosAutorizacao) {
        this.listaPeriodosAutorizacao = new ArrayList<>(listaPeriodosAutorizacao.listaPeriodosAutorizacao);
    }

    /**
     * Devolve a lista de periodos de autorização.
     *
     * @return lista de periodos de autorização.
     */
    public List<PeriodoAutorizacao> getlistaPeriodosAutorizacao() {
        return new ArrayList<>(this.listaPeriodosAutorizacao);
    }

    /**
     * Modifica a lista de periodos de autorização.
     *
     * @param listaPeriodosAutorizacao lista de periodos de autorização.
     */
    public void setListaPeriodosAutorizacao(List<PeriodoAutorizacao> listaPeriodosAutorizacao) {
        this.listaPeriodosAutorizacao = listaPeriodosAutorizacao;
    }

    /**
     * Cria um novo periodo de autorização.
     *
     * @param equipamento equipamento.
     * @param periodoAutorizacao periodoAutorizacao
     * @return periodo de autorização criado
     */
    public PeriodoAutorizacao novoPeriodoAutorizacao(Equipamento equipamento, PeriodoAutorizacao periodoAutorizacao) {

        return null;//alterar
    }

    /**
     * Verifica se um periodo de autorização é válido.
     *
     * @param periodoAutorizacao periodo de autorização a ser verificado
     * @return true se for válida, false caso contrário
     */
    public boolean validarPeriodosAutorização(PeriodoAutorizacao periodoAutorizacao) {

        return (!this.listaPeriodosAutorizacao.contains(periodoAutorizacao));
    }

    /**
     * Remove um periodo de autorização da lista.
     *
     * @param periodoAutorizacao periodo de autorização a ser removido
     * @return true se for removido com sucesso
     */
    public boolean removerPeriodoAutorizacao(PeriodoAutorizacao periodoAutorizacao) {
        return listaPeriodosAutorizacao.remove(periodoAutorizacao);
    }

    /**
     * Devolve a representação textual de todos os atributos da lista de
     * periodos de autorização.
     *
     * @return representação textual de todos os atributos da lista de periodos
     * de autorização.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("ListaPeriodosAutorizacao{ ");
        for (PeriodoAutorizacao periodoAutorizacao : listaPeriodosAutorizacao) {
            s.append(String.format("%s%n", periodoAutorizacao));
        }
        s.append("}");
        return s.toString();
    }

    /**
     * Compara se outro objeto é igual a esta listaPeriodosAutorização.
     *
     * @param outroObjeto objeto a comparar
     * @return true se forem iguais. False caso contrário
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        ListaPeriodosAutorizacao outraListaPeriodosAutorizacao = (ListaPeriodosAutorizacao) outroObjeto;

        return listaPeriodosAutorizacao.equals(outraListaPeriodosAutorizacao.listaPeriodosAutorizacao);
    }
}

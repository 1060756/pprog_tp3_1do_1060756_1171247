package org.dei.esoft.model;

/**
 *
 * @author 1060756_1171247
 */
public class PerfilAutorizacao {

    private String identificacao;
    private String descricao;

    private static final String IDENTIFICACAO_POR_OMISSAO = "sem identificador";
    private static final String DESCRICAO_POR_OMISSAO = "sem descricao";

    public PerfilAutorizacao(String identificacao, String descricao) {
        this.identificacao = identificacao;
        this.descricao = descricao;
    }

    public PerfilAutorizacao() {
        identificacao = IDENTIFICACAO_POR_OMISSAO;
        descricao = DESCRICAO_POR_OMISSAO;
    }

    public String getIdentificacao() {
        return this.identificacao;
    }

    public final void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public final void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ListaPeriodosAutorizacao novaListaPeriodosAutorizacao() {
        return new ListaPeriodosAutorizacao();
    }

    @Override
    public String toString() {
        return identificacao + ";" + descricao;
    }

    public boolean valida(String identificador) {
        if (this.descricao.length() == 0) {
            return false;
        } else {
            return identificador.compareTo(this.identificacao) != 0;
        }
    }

    public boolean adicionarEquipamento(Equipamento equipamento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean adicionarPeriodoAutorizacao(PeriodoAutorizacao periodoAutorizacao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

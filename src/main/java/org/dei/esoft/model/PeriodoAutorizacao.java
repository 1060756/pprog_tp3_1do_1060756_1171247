package org.dei.esoft.model;

/**
 *
 * @author 1060756_1171247
 */
public class PeriodoAutorizacao {

    private String diaSemana;
    private int horaInicio;
    private int horaFim;

    private static final String DIASEMANA_POR_OMISSAO = "Segunda";
    private static final int HORAINICIO_POR_OMISSAO = 0;
    private static final int HORAFIM_POR_OMISSAO = 23;

    public PeriodoAutorizacao(String diaSemana, int horaInicio, int horaFim) {
        this.diaSemana = diaSemana;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }

    public PeriodoAutorizacao() {
        diaSemana = DIASEMANA_POR_OMISSAO;
        horaInicio = HORAINICIO_POR_OMISSAO;
        horaFim = HORAFIM_POR_OMISSAO;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public int getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(int horaFim) {
        this.horaFim = horaFim;
    }

    @Override
    public String toString() {
        return "Periodo Autorizacao: " + diaSemana + ";" + horaInicio + ";" + horaFim;
    }
    

}

package org.dei.esoft.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Responsável por armazenar e gerir equipamentos.
 *
 * @author 1060756_1171247
 */
public class RegistoEquipamentos implements Serializable {

    /**
     * Lista dos equipamentos.
     */
    private List<Equipamento> listaEquipamentos;

    /**
     * Constrói uma instância de registo de equipamentos.
     */
    public RegistoEquipamentos() {
        listaEquipamentos = new ArrayList<>();
    }

    /**
     * Constrói uma instância de registo de equipamentos recebendo a lista de
     * equipamentos.
     *
     * @param listaEquipamentos lista de equipamentos
     */
    public RegistoEquipamentos(List<Equipamento> listaEquipamentos) {
        this.listaEquipamentos = new ArrayList<>(listaEquipamentos);
    }

    /**
     * Constrói uma instância de registo de equipamentos copiando outro registo
     * de equipamentos.
     *
     * @param registoEquipamentos registo de equipamentos a ser copiado
     */
    public RegistoEquipamentos(RegistoEquipamentos registoEquipamentos) {
        this.listaEquipamentos = new ArrayList<>(registoEquipamentos.listaEquipamentos);
    }

    /**
     * Devolve a lista de equipamentos.
     *
     * @return lista de equipamentos
     */
    public List<Equipamento> getListaEquipamentos() {
        return listaEquipamentos;
    }

    /**
     * Modifica a lista de equipamentos
     *
     * @param listaEquipamentos lista de equipamentos
     */
    public void setListaEquipamentos(List<Equipamento> listaEquipamentos) {
        this.listaEquipamentos = listaEquipamentos;
    }

    public Equipamento getEquipamento(String identificacaoEquipamento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Devolve a representação textual de todos os atributos do registo de
     * equipamentos.
     *
     * @return representação textual de todos os atributos do registo de
     * equipamentos
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("RegistoEquipamentos{");
        for (Equipamento equipamento : listaEquipamentos) {
            s.append(String.format("%s%n", equipamento));
        }
        s.append("}");
        return s.toString();
    }

    /**
     * Compara se outro objeto é igual a este RegistoEquipamentos.
     *
     * @param outroObjeto objeto a comparar
     * @return true se forem iguais. False caso contrário
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        RegistoEquipamentos outroRegistoEquipamentos = (RegistoEquipamentos) outroObjeto;

        return listaEquipamentos.equals(outroRegistoEquipamentos.listaEquipamentos);
    }

}

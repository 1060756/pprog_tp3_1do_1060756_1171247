package org.dei.esoft.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Responsável por armazenar e gerir perfis de autorização.
 *
 * @author 1060756_1171247
 */
public class RegistoPerfisAutorizacao implements Serializable {

    /**
     * Lista dos perfis de autorização.
     */
    private List<PerfilAutorizacao> listaPerfisAutorizacao;

    /**
     * Constrói uma instância de registo de perfis de autorização.
     */
    public RegistoPerfisAutorizacao() {
        listaPerfisAutorizacao = new ArrayList<>();
    }

    /**
     * Constrói uma instância de registo de perfis de autorização recebendo a
     * lista de perfis de autorização.
     *
     * @param listaPerfisAutorizacao lista de perfis de autorização
     */
    public RegistoPerfisAutorizacao(List<PerfilAutorizacao> listaPerfisAutorizacao) {
        this.listaPerfisAutorizacao = new ArrayList<>(listaPerfisAutorizacao);
    }

    /**
     * Constrói uma instância de registo de perfis de autorização copiando outro
     * registo de perfis de autorização.
     *
     * @param registoPerfisAutorizacao registo de perfis de autorização a ser
     * copiado
     */
    public RegistoPerfisAutorizacao(RegistoPerfisAutorizacao registoPerfisAutorizacao) {
        this.listaPerfisAutorizacao = new ArrayList<>(registoPerfisAutorizacao.listaPerfisAutorizacao);
    }

    /**
     * Devolve a lista de perfis de autorização.
     *
     * @return lista de perfis de autorização
     */
    public List<PerfilAutorizacao> getListaPerfisAutorizacao() {
        return listaPerfisAutorizacao;
    }

    /**
     * Modifica a lista de perfis de autorização
     *
     * @param listaPerfisAutorizacao lista de perfis de autorização
     */
    public void setListaPerfisAutorizacao(List<PerfilAutorizacao> listaPerfisAutorizacao) {
        this.listaPerfisAutorizacao = listaPerfisAutorizacao;
    }

    public PerfilAutorizacao novoPerfilAutorizacao() {
        return new PerfilAutorizacao();
    }

    /**
     * Devolve a representação textual de todos os atributos do registo de
     * perfis de autorização.
     *
     * @return representação textual de todos os atributos do registo de perfis
     * de autorização
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("RegistoPerfisAutorizacao{");
        for (PerfilAutorizacao perfilAutorizacao : listaPerfisAutorizacao) {
            s.append(String.format("%s%n", perfilAutorizacao));
        }
        s.append("}");
        return s.toString();
    }

    /**
     * Compara se outro objeto é igual a este RegistoPerfisAutorização.
     *
     * @param outroObjeto objeto a comparar
     * @return true se forem iguais. False caso contrário
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        RegistoPerfisAutorizacao outroRegistoPerfisAutorizacao = (RegistoPerfisAutorizacao) outroObjeto;

        return listaPerfisAutorizacao.equals(outroRegistoPerfisAutorizacao.listaPerfisAutorizacao);
    }

}

package org.dei.esoft.ui;

import org.dei.esoft.controller.EspecificarEquipamentoController;
import org.dei.esoft.model.Empresa;
import org.dei.esoft.utils.Utils;

/**
 *
 * @author 1060756_1171247
 */
public class EspecificarEquipamentoUI {

    private Empresa empresa;
    private EspecificarEquipamentoController especificarEquipamentoController;

    public EspecificarEquipamentoUI(Empresa empresa) {
        this.empresa = empresa;
        especificarEquipamentoController = new EspecificarEquipamentoController(empresa);
    }

    public void run() {
        System.out.println("\nNovo Equipamento:");
        especificarEquipamentoController.novoEquipamento();

        introduzDados();
        apresentaDados();

        if (Utils.confirma("Confirma os dados do Equipamento? (S/N)")) {
            if (especificarEquipamentoController.registaEquipamento()) {
                System.out.println("Equipamento registado.");
            } else {
                System.out.println("Equipamento não registado.");
            }
        }
    }

    private void introduzDados() {
        String descricao = Utils.readLineFromConsole("Introduza Descricão: ");
        String enderecoLogico = Utils.readLineFromConsole("Introduza Endereço Lógico: ");
        String enderecoFisico = Utils.readLineFromConsole("Introduza Endereço Fisico: ");
        String ficheiro = Utils.readLineFromConsole("Introduza Ficheiro Configuração: ");

        especificarEquipamentoController.setDados(descricao, enderecoLogico, enderecoFisico, ficheiro);
    }

    private void apresentaDados() {
        System.out.println("\nEquipamento:\n" + especificarEquipamentoController.getEquipamentoToString());
    }
}

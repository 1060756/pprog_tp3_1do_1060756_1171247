package org.dei.esoft.ui;

import org.dei.esoft.model.Empresa;

/**
 *
 * @author 1060756_1171247
 */
public class Main {

    public static void main(String[] args) {
        try {
            Empresa empresa = new Empresa();

            MenuUI uiMenu = new MenuUI(empresa);

            uiMenu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

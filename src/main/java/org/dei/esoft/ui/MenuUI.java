package org.dei.esoft.ui;

import java.io.IOException;
import org.dei.esoft.model.Empresa;
import org.dei.esoft.utils.Utils;

/**
 *
 * @author 1060756_1171247
 */
public class MenuUI {

    private Empresa empresa;
    private String opcao;

    public MenuUI(Empresa empresa) {
        this.empresa = empresa;
    }

    public void run() throws IOException {
        do {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Especificar Equipamento");

            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                RegistarPerfilAutorizacaoUI ui = new RegistarPerfilAutorizacaoUI(empresa);
                ui.run();
            }
            // Incluir as restantes opções aqui
        } while (!opcao.equals("0"));
    }
}

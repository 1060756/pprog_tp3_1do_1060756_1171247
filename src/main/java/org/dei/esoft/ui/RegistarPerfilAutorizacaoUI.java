package org.dei.esoft.ui;

import org.dei.esoft.controller.RegistarPerfilAutorizacaoController;
import org.dei.esoft.model.Empresa;
import org.dei.esoft.utils.Utils;
import java.util.Scanner;
import org.dei.esoft.controller.EspecificarEquipamentoController;

/**
 *
 * @author 1060756_1171247
 */
public class RegistarPerfilAutorizacaoUI {

    private Empresa empresa;
    private EspecificarEquipamentoController EquipamentoController;
    private RegistarPerfilAutorizacaoController RegistarPerfilAutorizacaoController;

    Scanner in = new Scanner(System.in);

    public RegistarPerfilAutorizacaoUI(Empresa empresa) {
        this.empresa = empresa;
        this.EquipamentoController = new EspecificarEquipamentoController(empresa);
        this.RegistarPerfilAutorizacaoController = new RegistarPerfilAutorizacaoController(empresa);
    }

    public void run() {
        System.out.println("\nNovo Perfil de Autorizacao:");
        RegistarPerfilAutorizacaoController.novoPerfilAutorizacao();

        introduzDados();
        apresentaDados();

        if (Utils.confirma("Confirma os dados do Perfil de Autorização? (S/N)")) {
            if (RegistarPerfilAutorizacaoController.validaPerfilAutorizacao()) {
                System.out.println("Perfil de autorização registado.");
            } else {
                System.out.println("Perfil de autorização não registado.");
            }
        }
    }

    private void introduzDados() {

        int hora_inicio;
        int hora_fim;
        boolean diaSemanaValido;
        boolean ultimoPeriodo;
        boolean ultimoEquipamento;
        Object opcao;
        String diaSemana;

        String identificador = Utils.readLineFromConsole("Introduza o identificador: ");
        String descricao = Utils.readLineFromConsole("Introduza a descrição do perfil: ");
        RegistarPerfilAutorizacaoController.setDados(identificador, descricao);

        if (RegistarPerfilAutorizacaoController.validaPerfilAutorizacao() == false) {
            System.out.println("ERRO: Descrição não preenchida ou identificador já existente.");
        } else {
            System.out.println("Seleccione os equipamentos a que o perfil de autorização terá acesso:");
            do {
                ultimoEquipamento = false;
                EquipamentoController.getListaEquipamentosToString();
                do {
                    opcao = Utils.selecionaObject(EquipamentoController.getListaEquipamentosToString());
                    if (opcao == null) {
                        System.out.println("Tente novamente:");
                    }
                } while (opcao == null);
                this.RegistarPerfilAutorizacaoController.novoEquipamentoAutorizado((String) EquipamentoController.getEnderecoLogico(EquipamentoController.getListaEquipamentos().get((int) opcao)));
                do {
                    ultimoPeriodo = false;
                    diaSemana = Utils.readLineFromConsole("Introduza o dia da semana: ");
                    do {
                        System.out.println("Introduza as horas permitidas:\nHora início: ");
                        hora_inicio = in.nextInt();
                    } while (hora_inicio < 0 || hora_inicio > 23);
                    do {
                        System.out.println("Hora fim: ");
                        hora_fim = in.nextInt();
                    } while (hora_fim < 0 || hora_fim > 23);
                    if (!Utils.confirma("Deseja associar mais algum período de autorização ao equipamento em questão? (S/N)")) {
                        ultimoPeriodo = true;
                        //this.RegistarPerfilAutorizacaoController.PeriodoAutorizacao.setDadosPeriodo(diaSemana, hora_inicio, hora_fim);
                    }
                } while (ultimoPeriodo == false);
                if (!Utils.confirma("Deseja associar mais algum equipamento ao perfil de autorização? (S/N)")) {
                    ultimoEquipamento = true;
                    System.out.println("Perfil de autorização registado com sucesso");
                }
            } while (ultimoEquipamento == false);

        }
    }

    private void apresentaDados() {
        System.out.println("\nPerfil de autorizacao:\n" + RegistarPerfilAutorizacaoController.getPerfilAutorizacaoString());
    }
}
